import pandas as pd
import textwrap
import warnings
import time
warnings.simplefilter("ignore")

# On Linux: use ctl-z to background. Else, change reading time in last loop.

pd_verbs = pd.read_pickle("pickles/pos_verb_pickle")
lemma = input("Please enter lemma: ")
pd_vvfin = pd_verbs[pd_verbs["Verbs String"].str.contains("VVFIN")]
pd_vvfin["VVFIN Tuples"] = pd_vvfin["POS Tags"].apply(
    lambda x: [y for y in x if y[2] == "VVFIN"])
pd_vvfin["VVFIN Lemmata"] = pd_vvfin["VVFIN Tuples"].apply(
    lambda x: [y[1] for y in x])
pd_vvfin["Lemma Bool"] = pd_vvfin["VVFIN Lemmata"].apply(
    lambda x: lemma in x)
result_pd = pd_vvfin[pd_vvfin["Lemma Bool"] ==True]
result_pd["Output"] = result_pd["Acronym work"] + ": " + result_pd["Sentence"]
result_list = result_pd["Output"].to_list()
for sent in result_list:
    textwrap.fill(sent)
    print(sent)
    # You can change the time it takes to show the next sentence here:
    time.sleep(10)

